from enum import Enum

import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import ENUM

metadata = sa.MetaData()


class BuildingMaterialTypeEnum(str, Enum):
    brick = "brick"
    wood = "wood"
    monolitic = "monolitic"
    panel = "panel"
    block = "block"
    brick_monolitic = "brick-monolitic"
    stalin = "stalin"


BuildingMaterialTypeDBEnum = ENUM(
    BuildingMaterialTypeEnum, name="house_material_type", metadata=metadata
)


class HeatSupplyTypeEnum(str, Enum):
    autonomous = "autonomous"
    individual = "individual"


HeatSupplyTypeDBEnum = ENUM(
    HeatSupplyTypeEnum, name="heat_supply_type", metadata=metadata
)


House = sa.Table(
    "House",
    metadata,
    sa.Column("id", sa.BigInteger, primary_key=True),
    sa.Column("year_release", sa.SmallInteger),
    sa.Column("floor_max", sa.SmallInteger),
    sa.Column("Entrances", sa.SmallInteger),
    sa.Column("flat_count", sa.SmallInteger),
    sa.Column("is_demolished", sa.Boolean),
    sa.Column("is_emergency", sa.Boolean),
    sa.Column("material_type", BuildingMaterialTypeDBEnum),
    sa.Column("heat_supply_type", HeatSupplyTypeDBEnum),
    sa.Column("Address", sa.Text),
    sa.Column("Views", sa.Integer, default=0),
)
