Since test assignment was put in a way that i can only ask What and not How i should do, 
there was a lot of concerns about my work, i will present them here. 
You should this of it how i think/what i do/what i question.

Software versions:

Requirements were in major.minor.revision+ format, the upper bound was added.
So versions more be like >=major.minor.revision, <major.minor + 1

Database:

`material_type` and `heat_supply_type` are marked as `Enum` not `varchar` or `str`,
so in database it's also `Enum`.

`material_type` probably should be `house_material` as it's done in current Cian search

Create database

```sql
CREATE DATABASE "cian-assignment-test";

CREATE ROLE "cian-assignment-test" WITH LOGIN PASSWORD 'cian-assignment-test';

GRANT ALL ON DATABASE "cian-assignment-test" to "cian-assignment-test";
```
