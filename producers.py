import logging

from pika.adapters.tornado_connection import TornadoConnection

LOG_FORMAT = (
    "%(levelname) -10s %(asctime)s %(name) -30s %(funcName) "
    "-35s %(lineno) -5d: %(message)s"
)
LOGGER = logging.getLogger(__name__)


class HouseEventProducer:
    EXCHANGE: str = "house_events"
    EXCHANGE_TYPE: str = "topic"
    ROUTING_KEY = "house.view"

    def __init__(self, ioloop, pika_conn_params):
        self._connection = None
        self._channel = None
        self._ioloop = ioloop
        self._pika_conn_params = None

    def connect(self):
        return TornadoConnection(
            parameters=self._pika_conn_params,
            on_open_callback=self.on_connection_open,
            custom_ioloop=self._ioloop,
        )

    def close_connection(self):
        LOGGER.info("Closing connection")
        self._connection.close()

    def add_on_connection_close_callback(self):
        LOGGER.info("Adding connection close callback")
        self._connection.add_on_close_callback(self.on_connection_closed)

    def on_connection_closed(self, connection, reason):
        self._channel = None
        LOGGER.warning("Connection closed, reopening in 5 seconds: %s", reason)
        self._connection.ioloop.call_later(5, self.reconnect)

    def on_connection_open(self, unused_connection):
        LOGGER.info("Connection opened")
        self.add_on_connection_close_callback()
        self.open_channel()

    def reconnect(self):
        self._connection = self.connect()

    def add_on_channel_close_callback(self):
        LOGGER.info("Adding channel close callback")
        self._channel.add_on_close_callback(self.on_channel_closed)

    def on_channel_closed(self, channel, reason):
        LOGGER.warning("Channel %i was closed: %s", channel, reason)
        self._connection.close()

    def on_channel_open(self, channel):
        LOGGER.info("Channel opened")
        self._channel = channel
        self.add_on_channel_close_callback()
        self.setup_exchange(self.EXCHANGE)

    def setup_exchange(self, exchange_name):
        LOGGER.info("Declaring exchange %s", exchange_name)
        self._channel.exchange_declare(exchange_name, durable=True)

    def add_on_cancel_callback(self):
        LOGGER.info("Adding consumer cancellation callback")
        self._channel.add_on_cancel_callback(self.on_consumer_cancelled)

    def close_channel(self):
        LOGGER.info("Closing the channel")
        self._channel.close()

    def open_channel(self):
        LOGGER.info("Creating a new channel")
        self._connection.channel(on_open_callback=self.on_channel_open)

    def publish_message(self, body):
        LOGGER.info(f"Publishing message {body}")
        self._channel.basic_publish(self.EXCHANGE, self.ROUTING_KEY, body)

    def run(self):
        self._connection = self.connect()
