import typing


class House(typing.NamedTuple):
    id: int
    year_release: typing.Optional[int]
    floor_max: typing.Optional[int]
    Entrances: typing.Optional[int]
    flat_count: typing.Optional[int]
    is_demolished: typing.Optional[bool]
    is_emergency: typing.Optional[bool]
    material_type: typing.Optional[str]
    heat_supply_type: typing.Optional[str]
    Address: str
    Views: int = 0
