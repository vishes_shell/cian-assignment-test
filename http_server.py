import aio_pika
import pika
import tornado.ioloop
import tornado.locks
import tornado.options
import tornado.web
from aio_pika import connect_robust
from aiopg.sa import create_engine
from tornado.options import define, options
from tornado.web import Application

import db
import producers

define("port", default=8888, help="run on the given port", type=int)
define(
    "db_url",
    default="postgresql://cian-assignment-test:cian-assignment-test@localhost:5433/cian-assignment-test",
    help="db dsn",
)
define("rabbit_url", default="amqp://guest:quest@localhost:5672", help="rabbitmq dsn")


class HouseDetailHandler(tornado.web.RequestHandler):
    async def get(self, house_id):
        db_engine = self.application.settings["db_engine"]
        async with db_engine.acquire() as conn:
            query = db.House.select().where(db.House.c.id == house_id)
            result = await conn.execute(query)
            row = await result.fetchone()

            if not row:
                raise tornado.web.HTTPError(404)

            self.write(dict(row))
        await self.application.settings["house_events_exchange"].publish(
            aio_pika.Message(str(house_id).encode()), routing_key="house.view"
        )


async def main():
    tornado.options.parse_command_line()

    async with create_engine(options.db_url, echo=True) as db_engine:
        amqp_connection = await connect_robust()
        channel = await amqp_connection.channel()
        routing_key = "house.view"
        exchange = await channel.declare_exchange("house_events", durable=True)
        queue = await channel.declare_queue("house.increase_views", durable=True)
        await queue.bind(exchange, routing_key)

        app = Application(
            [(r"/api/v1/houses/(\d+)/", HouseDetailHandler)],
            amqp_connection=amqp_connection,
            house_events_exchange=exchange,
            db_engine=db_engine,
            debug=True,
        )
        app.listen(options.port)
        shutdown_event = tornado.locks.Event()
        await shutdown_event.wait()


if __name__ == "__main__":
    tornado.ioloop.IOLoop.current().run_sync(main)
