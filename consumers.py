import pika
import sqlalchemy as sa

from db import House

connection = pika.BlockingConnection(pika.ConnectionParameters(host="localhost"))
channel = connection.channel()

channel.queue_declare("house.view", durable=True)
channel.queue_bind(queue="house.view", exchange="house_events")
print("Waiting for messages")

engine = sa.create_engine(
    "postgresql://cian-assignment-test:cian-assignment-test@localhost:5433/cian-assignment-test"
)


def callback(ch, method, properties, body):
    print(f"Received {body}")
    with engine.connect() as conn:
        conn.execute(
            House.update(House.c.id == int(body)).values(Views=House.c.Views + 1)
        )
    print(f"increased {int(body)}")
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue="house.view", on_message_callback=callback)

channel.start_consuming()
